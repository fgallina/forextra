# forextra

Small foreign exchange trade app

# Requirements

To run this project you need to install `docker` and
`docker-composer`.

[Docker install instructions](https://docs.docker.com/install/):
[Docker-compose install instructions](https://docs.docker.com/compose/install/):

Optionally you need to install GNU Make to use the utility Makefiles
inside each service folder.

# How to run it

At the root of the project run:

```
./scripts/start
```

This will build/download images and bring up all services. Once
started, visit the UI at `http://localhost:3000`.

You can check the startup state with `docker-compose logs -f`.

Finally a convenience script stop all containers is also provided:

```
./scripts/stop
```
# Important URLs

- UI: `http://localhost:3000`
- API root: `http://localhost:3000/api/`
- API docs: `http://localhost:3000/api/docs/`

# Structure

The project is split in two main parts, the first one is the `backend`
which serves the API endpoints and the other is the `frontend` serving
the React application for trade booking.

## Development environment details

The `frontend` container running at `http://localhost:3000` proxies
requests under `/api` to the `backend`. The reason to use this
internal proxying is to avoid requiring CORS. It also in a way mimics
what an ingress would do in a k8s cluster.

## Running tests, linters and more

There are self-documented convenience Makefiles inside the `backend`
and `frontend` folders. For example to run tests in either the
`frontend` or `backend` you could issue `make test`. Type `make` for
documentation on all available targets for each service.

## Where's the DB?

For simplicity the DB is currently SQLite and is stored at
`backend/config/db.sqlite3` and it will be automatically created if it
doesn't exists.

## Configuration overrides

In order to avoid draining fixer.io request limits in development, the
system is configured by default to use a fake static response for rate
calculation. The `FIXERIO_API_USE_CANNED_RESPONSES` controls this. In
order to setup a custom API key to hit the real fixer.io endpoint, use
the `FIXERIO_API_ACCESS_KEY` environment variable and set
`FIXERIO_API_USE_CANNED_RESPONSES` to `false`. Below is a full example
to enable `fixer.io` in development.

```
export FIXERIO_API_USE_CANNED_RESPONSES=false
export FIXERIO_API_ACCESS_KEY="key"
./scripts/start
```

# Caveats

## Careless rounding everywhere

No strict rules around rounding have been implemented in either the
backend nor the frontend. Probably the backend should be the source of
truth for all money calculations. Alternatively something like
[decimal.js](https://github.com/MikeMcl/decimal.js) could help.

## There's no protection around rate changes/conflicts on booking creation

If a given rate changed between the time of frontend display and the
backend booking creation, the user would just get the backend's value.

Ideally we could return a 409 Conflict HTTP response and make the
frontend smart enough to ask the user for confirmation in order to
proceed with the new rate.

This would be useful depending on the rate of change of data which
depends on the fixer.io hired plan. Rate of changes may be daily,
every hour, 10-minutes or every minute.

## No optimizations on the fixer.io integration

Fixer.io supports ETags but nothing has been implemented around
it. Moreover the current request filters to a single currency. If we
were to optimize things we would request all currencies for a given
base and then filter locally so that ETags hits would be more likely.

Another approach could be background tasks that took care of
periodically fetching and caching rates so that our code queries the
cache directly. This would effectively improve performance as there
would be no need to hit fixer.io via HTTP. The caveat of this is that
special care (monitoring!) must be taken so that the syncing process
doesn't drift away to much from fixer.io update rate.

## The supported currencies could be cached at frontend

We could code-generate the list for both the backend and frontend and
then the system would require one less request to render the trade
creation form.

Note: we could also cache the currencies response the first time so
that further renders don't hit the API.

## Development Fixer.io canned responses are static

A better approach for development would be to run a local instance of
fixer.io with carefully crafted data. I found out about their [Docker
container](https://github.com/fixerAPI/fixer) being available too late
in the process.

An easy path with what we have would be to rotate the current canned
response base and recalculate all rates from there.

## The React components need improvement

- TradeCreateView is undertested
- Both TradeCreateView and TradeListView would benefit from splitting
  them into smaller components.
