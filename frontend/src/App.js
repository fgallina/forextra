import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TradeListView from './components/TradeListView';
import TradeCreateView from './components/TradeCreateView';


function NotFound() {
  return (
    <div className="NotFound">
      <h1>Not found.</h1>
    </div>
  );
}

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={TradeListView} />
        <Route exact path="/new" component={TradeCreateView} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
}

export default App;
