import React from 'react';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import fetcherr from '../../shared/fetcherr';


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

export function Trade(props) {
  return (
    <TableRow key={props.id}>
      <TableCell>{props.sell_currency}</TableCell>
      <TableCell>{props.sell_amount}</TableCell>
      <TableCell>{props.buy_currency}</TableCell>
      <TableCell>{props.buy_amount}</TableCell>
      <TableCell>{props.rate}</TableCell>
      <TableCell>{new Date(props.date_booked).toLocaleString()}</TableCell>
    </TableRow>
  );
}

class TradeListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trades: [],
      error: null,
      isLoaded: false,
    };
  }

  componentDidMount() {
    // redux-less recommended approach from
    // https://reactjs.org/docs/faq-ajax.html
    fetcherr(
      "/api/trades/",
      {
        successHandler: (result) => {
          this.setState({
            isLoaded: true,
            trades: result
          });
        },
        errorHandler: (error) => {
          this.setState({
            error: error
          });
        },
      }
    );
  }

  render() {
    const { error, isLoaded, trades } = this.state;
    if (error) {
      return (
        <div className="{classes.root}">
          <h1>Error when fetching trades</h1>
          <p>{error.message}</p>
        </div>);
    }
    if (!isLoaded) {
      return (
        <div className="{classes.root}">
          <h1>Booked Trades</h1>
          {/* TODO: add nice spinner. */}
          <p>...</p>
        </div>
      );
    }
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Typography component="h1" variant="h3" gutterBottom>
          Booked Trades
        </Typography>
        <Divider />
        <Button
          style={{
            margin: 0,
            top: 30,
            right: 20,
            bottom: 'auto',
            left: 'auto',
            position: 'fixed',
          }}
          variant="contained"
          className={classes.button}
          onClick={() => this.props.history.push("/new")}>
          New Trade
        </Button>
        <Paper>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Sell Currency</TableCell>
                <TableCell>Sell Amount</TableCell>
                <TableCell>Buy Currency</TableCell>
                <TableCell>Buy Amount</TableCell>
                <TableCell>Rate</TableCell>
                <TableCell>Date Booked</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {trades.map(trade => (
                <Trade {...trade} key={trade.id} />
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default withRouter(withStyles(styles)(TradeListView));
