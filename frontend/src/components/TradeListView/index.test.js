import React from "react";
import { FetchMock } from '@react-mock/fetch';
import { MemoryRouter as Router } from "react-router-dom";
import { mount, render } from "enzyme";

import TradeListView, { Trade } from "./index";

const newestTrade = {
  id: "TRXNEWEST",
  sell_currency: "EUR",
  sell_amount: "100.00",
  buy_currency: "USD",
  buy_amount: "111.67",
  rate: "1.116744",
  date_booked: "2019-05-20T18:54:44.115271Z",
};
const oldestTrade = {
  id: "TRXOLDEST",
  sell_currency: "EUR",
  sell_amount: "100.00",
  buy_currency: "USD",
  buy_amount: "111.67",
  rate: "1.116744",
  date_booked: "2019-05-20T18:54:44.115271Z",
};
const tradesReponse = [
  newestTrade,
  oldestTrade,
];

describe("TradeListView", () => {
  it('renders as loading while waiting for response', () => {
    const wrapper = render(
      <FetchMock options={{
        matcher: '/api/trades/',
        response: tradesReponse
      }}>
        <Router>
          <TradeListView />
        </Router>
      </FetchMock>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('p').text()).toBe('...');
  });

  it('renders fetched trades', async () => {
    const wrapper = mount(
      <FetchMock options={{
        matcher: '/api/trades/',
        response: tradesReponse
      }}>
        keyLength=0 stabilizes router snapshots
        <Router keyLength={0}>
          <TradeListView />
        </Router>
      </FetchMock>
    );
    // Add a promise at the end of the promises stack and wait for it.
    // With this cheap workaround we can then inspect the component
    // after the mock response has been loaded.
    await new Promise((resolve) => setImmediate(resolve));
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.contains(<Trade {...newestTrade} />)).toBe(true);
    expect(wrapper.contains(<Trade {...oldestTrade} />)).toBe(true);
  });

  it('handles fetch errors', async () => {
    const wrapper = mount(
      <FetchMock options={{
        matcher: '/api/trades/',
        response: {status: 500, body: 'server error'},
      }}>
        <Router keyLength={0}>
          <TradeListView />
        </Router>
      </FetchMock>
    );
    await new Promise((resolve) => setImmediate(resolve));
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('h1').text()).toBe('Error when fetching trades');
    expect(wrapper.find('p').text()).toBe('server error');
  });
});
