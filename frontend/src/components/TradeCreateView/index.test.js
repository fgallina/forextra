import React from "react";
import { FetchMock } from '@react-mock/fetch';
import { MemoryRouter as Router } from "react-router-dom";
import { render } from "enzyme";

import TradeCreateView from "./index";

const currenciesReponse = {
  "currencies": [
    ["USD", "United States Dollar (USD)"],
    ["EUR", "Euro (EUR)"]
  ]
};

describe("TradeCreateView", () => {
  it('renders as loading while waiting for response', () => {
    const wrapper = render(
      <FetchMock options={{
        matcher: '/api/currencies/',
        response: currenciesReponse
      }}>
        <Router>
          <TradeCreateView />
        </Router>
      </FetchMock>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('p').text()).toBe('...');
  });
  // # TODO: tests missing
});
