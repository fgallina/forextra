import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Input from '@material-ui/core/Input';
import PropTypes from 'prop-types';
import React from 'react';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from "react-router-dom";

import fetcherr from '../../shared/fetcherr';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  form: {
    marginTop: theme.spacing.unit * 3,
  },
  formLabel: {
    marginBottom: theme.spacing.unit * 3,
  }
});

class TradeCreateView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currencies: [],
      error: null,
      isLoaded: false,
      submitted: false,
    };
    this.onCancel = this.onCancel.bind(this);
    this.onBuyCurrencyChange = this.onBuyCurrencyChange.bind(this);
    this.onSellCurrencyChange = this.onSellCurrencyChange.bind(this);
    this.onRateChange = this.onRateChange.bind(this);
    this.onSellAmountChange = this.onSellAmountChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.submitted = false;
  }

  blockSubmit(value) {
    // state set is async, we need synchronous blocking of the submit
    // button behavior. The state key with the same name is used for
    // re-rendering of UI elements that depend on it.
    this.submitted = value;
    this.setState({ submitted: value });
  }

  componentDidMount() {
    fetcherr(
      "/api/currencies/",
      {
        successHandler: (result) => {
          this.setState({
            isLoaded: true,
            currencies: result["currencies"]
          });
        },
        errorHandler: (error) => {
          this.setState({
            error: error,
          });
        },
      }
    );
  }

  onCancel() {
    this.props.history.push("/");
  }

  maybeUpdateRate() {
    const {buyCurrency, sellCurrency} =  this.state;
    if (buyCurrency && sellCurrency) {
      fetcherr(
        `/api/rates/${sellCurrency}/${buyCurrency}/`,
        {
          successHandler: (result) => {
            this.setState(
              {
                rate: result["rate"],
              },
              this.updateBuyAmount,
            );
          },
          errorHandler: (error) => {
            alert(error.message);
          },
        }
      );
      return;
    }
    this.setState({
      rate: "",
      buyAmount: "",
    });
  }

  onBuyCurrencyChange(event) {
    this.setState(
      { buyCurrency: event.target.value },
      this.maybeUpdateRate,
    );
  }

  onSellCurrencyChange(event) {
    this.setState(
      { sellCurrency: event.target.value },
      this.maybeUpdateRate,
    );
  }

  updateBuyAmount() {
    const {rate, sellAmount} =  this.state;
    if (rate && sellAmount) {
      this.setState({ buyAmount: (rate * sellAmount).toFixed(2) });
      return;
    }
    this.setState({ buyAmount: "" });
  }

  onSellAmountChange(event) {
    this.setState(
      { sellAmount: event.target.value },
      this.updateBuyAmount,
    );
  }

  onRateChange(event) {
    this.setState(
      { rate: event.target.value },
      this.updateBuyAmount,
    );
  }

  onSubmit(event) {
    event.preventDefault();
    if (this.submitted) {
      return;
    }
    this.blockSubmit(true);
    fetcherr(
      "/api/trades/",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(
          {
            sell_amount: this.state.sellAmount,
            buy_currency: this.state.buyCurrency,
            sell_currency: this.state.sellCurrency,
          }
        ),
        successHandler: (result) => {
          this.props.history.push("/");
        },
        errorHandler: (error) => {
          alert(error.message);
          this.blockSubmit(false);
        },
      }
    );
  }

  render() {
    const { error, isLoaded, currencies } = this.state;
    if (error) {
      return (
        <div className="{classes.root}">
          <h1>An error ocurred</h1>
          <p>{error.message}</p>
        </div>);
    }
    if (!isLoaded) {
      return (
        <div className="{classes.root}">
          <h1>New Trade</h1>
          {/* TODO: add nice spinner. */}
          <p>...</p>
        </div>
      );
    }

    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Typography component="h1" variant="h3" gutterBottom>
          New Trade
        </Typography>
        <Divider />
        <form onSubmit={this.onSubmit} className={classes.form}>
          <FormControl component="fieldset" className={classes.formControl}>
            <FormGroup>
              <FormControlLabel
                className={classes.formLabel}
                control={
                  <Select
                    native
                    tabindex={1}
                    onChange={this.onSellCurrencyChange}
                    value={this.state.sellCurrency}
                    name="sell_currency"
                  >
                    <option value="" />
                    {currencies.map(currency => (
                      <option key={currency[0]} value={currency[0]}>{currency[1]}</option>
                    ))}
                  </Select>
                }
                label="Sell currency"
                labelPlacement="top"
              />
              <FormControlLabel
                className={classes.formLabel}
                control={
                  <Input name="sell_amount"
                         tabindex={2}
                         type="text"
                         onChange={this.onSellAmountChange}
                         value={this.state.sellAmount}
                         required />
                }
                label="Sell amount"
                labelPlacement="top"
              />
            <Button tabindex={4} type="submit">Create</Button>
            </FormGroup>
          </FormControl>

          <FormControl component="fieldset" className={classes.formControl}>
            <FormGroup>
              <FormControlLabel
                className={classes.formLabel}
                control={
                  <Input name="rate"
                         type="text"
                         onChange={this.onRateChange}
                         value={this.state.rate}
                         disableUnderline={true}
                         disabled />
                }
                label="Rate"
                labelPlacement="top"
              />
            </FormGroup>
          </FormControl>

          <FormControl component="fieldset" className={classes.formControl}>
            <FormGroup>
              <FormControlLabel
                className={classes.formLabel}
                control={
                  <Select
                    native
                    tabindex={3}
                    onChange={this.onBuyCurrencyChange}
                    value={this.state.buyCurrency}
                    name="buy_currency"
                  >
                    <option value="" />
                    {currencies.map(currency => (
                      <option key={currency[0]} value={currency[0]}>{currency[1]}</option>
                    ))}
                  </Select>
                }
                label="Buy currency"
                labelPlacement="top"
              />
              <FormControlLabel
                className={classes.formLabel}
                control={
                  <Input name="buy_amount" type="text" value={this.state.buyAmount} disabled></Input>
                }
                label="Buy amount"
                labelPlacement="top"
              />
              <Button tabindex={5} onClick={this.onCancel}>Cancel</Button>
            </FormGroup>
          </FormControl>
        </form>
      </div>
    );
  }
}

TradeCreateView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(TradeCreateView));
