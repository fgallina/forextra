// wrapper around fetch to handle API errors in a awful but still
// normalized way.
const fetcherr = (url, opts={}) => {
  opts = opts || {};
  const successHandler = opts.successHandler || (() => {});
  const errorHandler = opts.errorHandler || (() => {});
  return fetch(url, opts)
    .then(
      (response) => {
        if (!response.ok) {
          return response.text();
        };
        return response.json();
      }
    )
    .then(
      (result) => {
        if (typeof result === "string") {
          // if result is a string it implies error.
          errorHandler(new Error(result));
          return;
        };
        successHandler(result);
      },
      (error) => {
        errorHandler(error);
      },
    );
};

export default fetcherr;
