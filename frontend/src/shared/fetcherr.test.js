import fetchMock from 'fetch-mock';

import fetcherr from "./fetcherr";

describe("fetcherr", () => {
  afterEach(() => {
    fetchMock.restore();
    jest.clearAllMocks();
  });

  it('calls the appropiate url', () => {
    fetchMock.getOnce("/test/url/", {
      status: 200,
      body: { body: 'body', hash: 'hash' }
    });
    return fetcherr("/test/url/").then(() => {
      expect(fetchMock.called("/test/url/", 'GET')).toBe(true);
    });
  });

  it('calls the success handler if given', () => {
    const handler = jest.fn();
    fetchMock.getOnce("/test/url/", {
      status: 200,
      body: { body: 'body', hash: 'hash' }
    });
    return fetcherr("/test/url/", { successHandler: handler }).then(() => {
      expect(handler).toBeCalled();
      expect(fetchMock.called("/test/url/", 'GET')).toBe(true);
    });
  });

  it('calls the error handler on non-ok codes', () => {
    const handler = jest.fn();
    fetchMock.getOnce("/test/url/", {
      status: 400,
      body: { body: 'body', hash: 'hash' }
    });
    return fetcherr("/test/url/", { errorHandler: handler }).then(() => {
      expect(handler).toBeCalled();
      expect(fetchMock.called("/test/url/", 'GET')).toBe(true);
    });
  });

  it('calls the error handler on exceptions', () => {
    const handler = jest.fn();
    fetchMock.getOnce("/test/url/", { status: new Error("bang!") });
    return fetcherr("/test/url/", { errorHandler: handler }).then(() => {
      expect(handler).toBeCalled();
      expect(fetchMock.called("/test/url/", 'GET')).toBe(true);
    });
  });
});
